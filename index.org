#+SETUPFILE: https://ppymdjr.gitlab.io/orgmode-setup/orgexport.setup
#+TITLE: Peninsula cardiology SpR training

* Introduction
Welcome to the Peninsula cardiology SpR training day website. Here you'll find information about upcoming training days, as well as useful external resources and training days, and recordings of previous sessions.

* Next Sessions
11th September 2020; Arrythmia Day; Full day at the RD+E, streamed live via MS Teams, available here afterwards

6th November 2020; Cardiac CT and CMR; Full day at the RD+E, streamed live via MS Teams, available here afterwards

Session 3 TBC

* Past Sessions
Recent sessions:

There will be links to pages here, one page per training day, with all the videos embedded like this:

The videos can be embedded straight from the teams account, they will work if the viewer logs into their teams account, and will only be visible to members of that microsoft teams group (I'll have versions of the talks saved elsewhere as well).

Or if we aren't worried about privacy we can just embed a youtube video like this

#+HTML: <iframe width="560" height="315" src="https://www.youtube.com/embed/xQ_IQS3VKjA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>